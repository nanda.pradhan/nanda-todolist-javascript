var element;
$(document).ready(function() {
    element= document.getElementById('task-element');
    display();
});

function editable(event) {
    this.contentEditable = 'true';
}

function create_element(task_id,task,status){
  
    var main_div = document.createElement("div");
    main_div.classList.add("main-div");
    main_div.setAttribute("id", task_id);

    var check_box_div = document.createElement("div");
    var check_box = document.createElement("input");
    check_box.setAttribute("type", "checkbox");
    check_box.classList.add("check-box");
    check_box.addEventListener("change", complete_task);
    check_box.setAttribute('id',"filled-in form-check-input");
    check_box.classList.add("check-box");
    check_box_div.appendChild(check_box);
    
    var task_div = document.createElement("div");
    var task_field = document.createElement("span");
    task_field.addEventListener('click', editable);
    task_field.addEventListener('blur', update);
    var node = document.createTextNode(task);
    task_field.appendChild(node)
            

    var delete_button_div = document.createElement("div")
    var delete_button = document.createElement("button");
    delete_button.classList.add("delete-button-style")
    delete_button.addEventListener('click', delete_task)
    delete_button_div.appendChild(delete_button)

    main_div.appendChild(check_box_div)
    main_div.appendChild(task_div)
    main_div.appendChild(delete_button_div)
    main_div.setAttribute('draggable',true)
    if(status)
    { 
        task_field.classList.add("strike-text")
        check_box.checked=true;
        main_div.classList.remove("main-div")
        main_div.classList.add("change-color")
    }
    task_div.appendChild(task_field)
    addDHandlers(main_div)
    element.insertBefore(main_div,element.firstChild)
}

function display(){
    element.innerHTML="";
    var data = JSON.parse(localStorage.getItem("tasks"));
    if(data!=null)
    {
        data.forEach(function(elements) {
            var task=elements["task"]
            var id=elements["id"]
            var status_value=elements["status"]
            create_element(id,task,status_value)    
        });  
    }    
}

function store() {
    var tsk = document.getElementById("task");
    var id_count=0
    var date = new Date();
    if (typeof localStorage["tasks"] == 'undefined')
     {
        var task_arr = [];
        var task = {
            'id': id_count,
            'order_id':id_count,
            'task': tsk.value,
            'create_date': date,
            'status': false,   
        };
        task_arr.push(task);
        localStorage.setItem("tasks", JSON.stringify(task_arr));
    } else {
        var id_count = (JSON.parse(localStorage.getItem("tasks"))).length
        var stored = JSON.parse(localStorage.getItem("tasks"));
        var task = {
            'id': id_count,
            'order_id':id_count,
            'task': tsk.value,
            'create_date': date,
            'status': false,
        };
        stored.push(task)
        localStorage.setItem("tasks", JSON.stringify(stored));
    }
}
function delete_task(event) 
{
    var id = this.parentElement.parentElement.id;
    var stored = JSON.parse(localStorage.getItem("tasks"));
    var index = 0;
    stored.forEach(function (element) {
        if (element["id"] == id) {
            if (element["status"]) {
                stored.splice(index, 1);

            } else {
                alert("please complete the task")
            }
        }
        index += 1;
    });
    localStorage.setItem("tasks", JSON.stringify(stored));
    display();
}
function update(event) 
{   
    var tsk = this.innerText
    var stored = JSON.parse(localStorage.getItem("tasks"));
    var index = 0;
    var id=this.parentElement.parentElement.id
    stored.forEach(function (element) {

        if (element["id"] == id) {
            task_obj = element;
            task_obj["task"] = tsk;
            stored.splice(index, 1, task_obj);
        }
        index += 1;
    });
    localStorage.setItem("tasks", JSON.stringify(stored));
    display();
}
function complete_task(event) {
    id = this.parentElement.parentElement.id
    var check_value = this.checked;
    var stored = JSON.parse(localStorage.getItem("tasks"));
    var index = 0;
    stored.forEach(function (element) {
        if (element["id"] == id) {
            if (check_value) {
                var task_obj = element;
                task_obj["status"] = check_value;
                stored.splice(index, 1, task_obj);
            } else {
                var task_obj = element;
                task_obj["status"] = check_value;
                stored.splice(index, 1, task_obj);
            }
        }
        index += 1;
    });
  localStorage.setItem("tasks", JSON.stringify(stored));
  display();
}
var source;
function handleDragStart(evt) {
    source = this;
    evt.dataTransfer.setData("text/plain", this.innerHTML);
    evt.dataTransfer.effectAllowed = "move";
}

function handleDragOver(evt) {
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "move";
}
function handleDrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    source.innerHTML = this.innerHTML;
    this.innerHTML = evt.dataTransfer.getData("text/plain");
    var stored = JSON.parse(localStorage.getItem("tasks"));
    var source_ele=stored[source.id]
    var dest_elemeny=stored[this.id]
    var temp=source_ele['order_id']
    source_ele['order_id']=dest_elemeny['order_id']
    dest_elemeny['order_id']=temp
    stored.splice(source.id, 1, dest_elemeny);
    stored.splice(this.id, 1, source_ele);
    localStorage.setItem("tasks", JSON.stringify(stored));
    display()
}
function addDHandlers(elem) 
{
    elem.addEventListener('dragstart', handleDragStart, false);
    elem.addEventListener('dragover', handleDragOver, false);
    elem.addEventListener('drop', handleDrop, false);
}
function clearAll()
{
    localStorage.removeItem("tasks")
    display()
}